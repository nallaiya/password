import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyFieldInput } from './password';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { IsDisabledDirective } from './is-disabled.directive';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule } from '@angular/common/http';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { environment } from '../environments/environment';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
@NgModule({
  declarations: [
    AppComponent,
    FormlyFieldInput,
    IsDisabledDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    ShowHidePasswordModule,
    AngularEditorModule,
    HttpClientModule,
    FormsModule,
    AccordionModule.forRoot(),
    FormlyModule.forRoot(
      {validationMessages: [
        { name: 'required', message: 'This field is required' },
      ],
        types: [
          { name: 'password', component: FormlyFieldInput,wrappers:['form-field'] },
        ],
      }
    ),
    FormlyBootstrapModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
