import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
 selector: 'formly-field-input',
 template: `
 <label>Enter the password</label>
<show-hide-password size="sm" btnStyle="primary" [btnOutline]="false">
<input type="password" name="password" class="passClass" [formControl]="formControl" [formlyAttributes]="field" autocomplete="new-password">
</show-hide-password> `,
})

export class FormlyFieldInput extends FieldType {
    

}
