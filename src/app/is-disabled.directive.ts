import { Directive, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appIsDisabled]'
})
export class IsDisabledDirective {
  @Input() appIsDisabled: any;

  constructor(public ele: ElementRef, private renderer: Renderer2) {

  }
  ngOnInit() {

  }
  ngDoCheck() {
    console.log("check", this.appIsDisabled)
    let style = this.ele.nativeElement
    if (this.appIsDisabled == true) {
      style.setAttribute('style', 'pointer-events: none;opacity: 0.5;')
    }
    else {
      style.setAttribute('style', 'pointer-events: auto')
    }
  }
}
