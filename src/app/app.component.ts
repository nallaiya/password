import { Component, ViewEncapsulation, ViewChild } from "@angular/core";
// import { FormGroup } from '@angular/forms';
// import { FormlyFieldConfig } from '@ngx-formly/core';
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { WindowServiceService } from "./window-service.service";
import { HostListener } from "@angular/core";
import { ColumnMode } from "@swimlane/ngx-datatable";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  diviceType: any;
  tabelData: any[] = [];
  screenWidth: any;
  encapsulation: ViewEncapsulation.None;
  expanded: any = {};
  timeout: any;
  ColumnMode = ColumnMode;

  @ViewChild("myTable") table: any;
  constructor() {
    this.tabelData.push(
      {
        name: "dinesh",
        last: "kanna",
        genter: "male",
        loc: "gudalur",
        reason: "null",
      },
      {
        name: "shree",
        last: "kanna",
        genter: "male",
        loc: "gudalur",
        reason: "null",
      },
      {
        name: "kiruba",
        last: "jhon",
        genter: "male",
        loc: "gudalur",
        reason: "null",
      },
      {
        name: "jhon",
        last: "kiruba",
        genter: "male",
        loc: "gudalur",
        reason: "null",
      },
      {
        name: "sathya",
        last: "dinesh",
        genter: "male",
        loc: "gudalur",
        reason: "null",
      }
    );
  }
  onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log("paged!", event);
    }, 100);
  }

  toggleExpandRow(row) {
    console.log("Toggled Expand Row!", row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log("Detail Toggled", event);
  }
  ngOnInit() {
    this.getScreenSize();
  }
  @HostListener("window:resize", ["$event"])
  getScreenSize(event?) {
    this.screenWidth = window.innerWidth;
  }
  ngDoCheck() {
    if (this.screenWidth > 960) {
      this.diviceType = "lg";
    } else if (this.screenWidth < 600) {
      this.diviceType = "sm";
    } else {
      this.diviceType = "md";
    }
  }

  // isDisabled: boolean = false
  // form = new FormGroup({});
  // model = {};
  // fields: FormlyFieldConfig[] = [
  //   {
  //     key: 'Password',
  //     type: 'password',
  //     templateOptions: {
  //       placeholder: ' Enter your password',
  //       required: true
  //     }
  //   }
  // ];
  // disabled() {
  //   this.isDisabled = true;
  // }
  // enabled(){
  //   this.isDisabled = false;
  // }
  // submit(event) {
  //   console.log(this.model);
  // }
}
